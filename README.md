# Avris Avi

Simple placeholder avatars

Free & simple API for generating nice, colorful default avatars based on a given string (like username or email).
Just put your base64-encoded identifier after our URL, optionally specify the desired size and file format, et voilà!

## Author

* **Andrea Vos** [avris.it](https://avris.it)
* Licence: [OQL](https://oql.avris.it/license?c=Andrea%20Vos%7Chttps://avris.it)
