install:
	composer install
	pnpm install

start:
	symfony run --daemon pnpm run dev-server
	symfony serve --daemon

stop:
	symfony server:stop

deploy:
	composer install --no-dev --optimize-autoloader
	pnpm install
	node_modules/.bin/encore production
	bin/console cache:clear
