<?php

namespace App\Controller;

use App\Service\AvatarGenerator;
use App\Service\ImageCache;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    private const FORMATS = [
        'png' => 'image/png',
        'jpg' => 'image/jpeg',
        'gif' => 'image/gif',
    ];

    /**
     * @Route("/", name="homepage")
     */
    public function homepage()
    {
        return $this->render('homepage.html.twig');
    }

    /**
     * @Route(
     *     "/{type}{size}/{identifier}.{format}",
     *     name="generate",
     *     requirements={
     *         "size"="\d+",
     *         "type"="letter-|shape-|",
     *         "identifier"="[A-Za-z0-9=_-]+",
     *         "format"="png|jpg|gif"
     *     },
     *     defaults={
     *         "type"="letter-",
     *         "format"="png"
     *     }
     * )
     */
    public function generate(ImageCache $imageCache, AvatarGenerator $generator, int $size, string $type, string $identifier, string $format)
    {
        $type = str_replace('-', '', $type) ?: 'letter'; // BC

        $image = $imageCache->fetch(
            sprintf('%s_%s_%s', $identifier, $type, $size),
            function () use ($generator, $identifier, $type, $size) {
                try {
                    return $generator->generate(
                        base64_decode(strtr($identifier, '-_', '+/')),
                        $type,
                        $size,
                    );
                } catch (\InvalidArgumentException $e) {
                    throw new BadRequestHttpException($e->getMessage(), $e);
                }
            }
        );

        return new Response(
            $image->get($format),
            200,
            [
                'content-type' => self::FORMATS[$format],
                'cache-control' => 'max-age=86400',
            ],
        );
    }
}
