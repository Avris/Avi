<?php

namespace App\Service;

use Imagine\Gd\Font;
use Imagine\Image\Box;
use Imagine\Image\Fill\Gradient\Vertical;
use Imagine\Image\ImageInterface;
use Imagine\Image\ImagineInterface;
use Imagine\Image\Palette\RGB;
use Imagine\Image\Point;

final class AvatarGenerator
{
    private ImagineInterface $imagine;
    private string $font;
    private array $shapes;
    private RGB $palette;
    private int $minSize;
    private int $maxSize;
    private float $textSize;
    private float $textPosX;
    private float $textPosY;

    public function __construct(
        ImagineInterface $imagine,
        string $font,
        string $shapes,
        int $minSize = 16,
        int $maxSize = 360,
        float $textSize = 0.7,
        float $textPosX = 0.5,
        float $textPosY = 0.5
    )
    {
        $this->imagine = $imagine;
        $this->palette = new RGB();
        $this->font = $font;
        $this->shapes = glob($shapes . '/shape-*.png');
        $this->minSize = $minSize;
        $this->maxSize = $maxSize;
        $this->textSize = $textSize;
        $this->textPosX = $textPosX;
        $this->textPosY = $textPosY;
    }

    public function generate(string $identifier, string $type, int $size): ImageInterface
    {
        if (!$identifier) {
            throw new \InvalidArgumentException('Text cannot be empty');
        } else if ($size < $this->minSize) {
            throw new \InvalidArgumentException(sprintf('Min size is %sx%s', $this->minSize, $this->minSize));
        } else if ($size > $this->maxSize) {
            throw new \InvalidArgumentException(sprintf('Max size is %sx%s', $this->maxSize, $this->maxSize));
        }

        $box = new Box($size, $size);
        $color = $this->palette->color(substr(md5($identifier), 0, 6));
        $fill = new Vertical($box->getHeight(), $color->darken(127), $color);

        $hero = $this->buildHero($type, $identifier, $size);

        $image = $this->imagine->create($box);
        $image->fill($fill);
        $image->paste($hero, $this->placeImage($image, $hero, $this->textPosX, $this->textPosY));

        return $image;
    }

    private function buildHero(string $type, string $identifier, int $size): ImageInterface
    {
        switch ($type) {
            case 'letter':
                return $this->buildLetter($identifier, $size);
            case 'shape':
                return $this->buildShape($identifier, $size);
            default:
                throw new \Exception(sprintf('Unrecognised type "%s', type));
        }
    }

    private function buildLetter(string $identifier, int $size): ImageInterface
    {
        $font = new Font(
            $this->font,
            $size * $this->textSize,
            $this->palette->color('fff')
        );

        $letter = mb_strtoupper(mb_substr($identifier, 0, 1));

        $textImage = $this->imagine->create(new Box($size, $size), $this->palette->color('000', 0));
        $textImage->draw()->text($letter, $font, new Point(0, 0));

        return $this->autoCrop($textImage);
    }

    private function buildShape(string $identifier, int $size): ImageInterface
    {
        $key = hexdec(substr(md5($identifier), 0, 8));
        $shape = $this->shapes[$key % count($this->shapes)];

        return $this->imagine
            ->open($shape)
            ->resize(new Box($size * $this->textSize, $size * $this->textSize))
        ;
    }

    private function autoCrop(ImageInterface $image): ImageInterface
    {
        $size = $image->getSize();
        $width = $size->getWidth();
        $height = $size->getHeight();

        $minX = $size->getWidth();
        $minY = $size->getHeight();
        $maxX = 0;
        $maxY = 0;

        for ($x = 0; $x < $width; $x++) {
            for ($y = 0; $y < $height; $y++) {
                if (!$image->getColorAt(new Point($x, $y))->getAlpha()) {
                    continue;
                }
                if ($x < $minX) {
                    $minX = $x;
                }
                if ($y < $minY) {
                    $minY = $y;
                }
                if ($x > $maxX) {
                    $maxX = $x;
                }
                if ($y > $maxY) {
                    $maxY = $y;
                }
            }
        }

        return $image->crop(new Point($minX, $minY), new Box($maxX - $minX + 1, $maxY - $minY + 1));
    }

    private function placeImage(ImageInterface $outer, ImageInterface $inner, float $posX, float $posY): Point
    {
        return new Point(
            ($outer->getSize()->getWidth() - $inner->getSize()->getWidth()) * $posX,
            ($outer->getSize()->getHeight() - $inner->getSize()->getHeight()) * $posY,
        );
    }
}
