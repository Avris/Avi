<?php

namespace App\Service;

use Imagine\Image\ImageInterface;
use Imagine\Image\ImagineInterface;
use Symfony\Component\Filesystem\Filesystem;

final class ImageCache
{
    private string $dir;
    private ImagineInterface $imagine;
    private Filesystem $filesystem;

    public function __construct(string $dir, ImagineInterface $imagine, Filesystem $filesystem)
    {
        $this->dir = $dir;
        $this->imagine = $imagine;
        $this->filesystem = $filesystem;

        $this->filesystem->mkdir($this->dir);
    }

    public function fetch(string $key, callable $generate): ImageInterface
    {
        $path = $this->dir . DIRECTORY_SEPARATOR . $key . '.png';

        if ($this->filesystem->exists($path)) {
            return $this->imagine->open($path);
        }

        /** @var ImageInterface $image */
        $image = $generate();

        $image->save($path);

        return $image;
    }
}
