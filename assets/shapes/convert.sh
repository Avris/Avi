#!/bin/bash

for svg_file in *.svg; do
    base_name="${svg_file%.svg}"

    echo "Converting $svg_file to $base_name.png"

    rsvg-convert -o "$base_name.png" "$svg_file"
done
