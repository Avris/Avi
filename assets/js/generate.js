import {_on, _sel} from 'avris-vanillin';
import { fromByteArray } from 'base64-js';

const $form = _sel('form');
const $image = _sel('.generate-image');
const $url = _sel('.generate-url');

const base64 = string => {
    const byteArray = string.split('').map(char => char.charCodeAt(0));
    return fromByteArray(byteArray).replace('+', '-').replace('/', '_').replace(/=+$/g, '');
};

const generate = _ => {
    const data = {};
    new FormData($form).forEach((value, key) => { data[key] = value });
    try {
        if (!data.identifier || !['letter', 'shape'].includes(data.type) || data.size < 16 || data.size > 360) {
            throw 'Invalid params';
        }

        const url = `${$form.dataset.base}/${data.type}-${data.size}/${base64(data.identifier)}.${data.format}`;

        $image.setAttribute('src', url);
        $url.value = url;
    } catch {
        $image.setAttribute('src', `${$form.dataset.base}/${data.type}-${data.size}/${base64('×')}.${data.format}`);
        $url.value = $form.dataset.invalid;
    }
};

_on('.generate-source', ['change', 'keyup'], generate);
generate();
